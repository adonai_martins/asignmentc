using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assignment;
using System.ComponentModel.DataAnnotations;
using System;
using System.Drawing;

namespace assignmentTests
{

    /// <summary>
    /// class that holds the tests
    /// </summary>
    [TestClass]
    public class UnitTest1
    {
        /// <summary>
        /// test that checks if getshape from factory class throws an exception
        /// </summary>
        [TestMethod]
        public void testGetShape_fromFactoryClass_throwsException()//checks if method throws exception
        {

            ShapeFactory shape = new ShapeFactory(); //creates an object of shapefactory

            int[] list = { 1, 2, 3, 4 }; //parameters list

            Assert.ThrowsException<System.ArgumentException>(() => shape.getShape("adsda", Color.Red, list));//checks if method throws exception
            
        }
    }
}
