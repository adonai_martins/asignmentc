﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment
{
    public partial class Form1 : Form
    {
        private static int screenWidth = 640;
        private static int screenHeight = 480;
        
        //bitmap to draw, which will be dropped in the output screen
        private Bitmap outPutBitMap = new Bitmap(screenWidth, screenHeight); //bit map of graphics to be outpit in the output window
        private Canvas canvas; //canvas object that handles the drawing
        private Parser parser;
        public Form1()
        {
            InitializeComponent();

            //initialises canvas and parser objects
            canvas = new Canvas(Graphics.FromImage(outPutBitMap));
            parser = new Parser(canvas);
            inputWindows.Text = FileHandler.load(); //loads the previous work
        }
        
        private void drawOutput(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImageUnscaled(outPutBitMap, 0, 0); //draws the object to the bitmap
        }

        private void runButton_Click(object sender, EventArgs e)
        {
            errorBox.Text = "";
            //these commands reset the pen color and fill so the program runs starting with default settings
            canvas.setPenColor("BLACK"); 
            canvas.fill(false);
            canvas.clearOutputWindow();//clears output window so more graphics can be added

            parser.setLines(inputWindows.Text);//passes the string to be parsed
            parser.parse();//check for errors and output the graphics
            outputWindows.Refresh(); //refreshes the windows so the graphics are loaded in it
            errorBox.Text = parser.getErrors();//sets the errorBox to a string of errors
            FileHandler.save(inputWindows.Text);
            parser.resetErrors();

        }


        /// <summary>
        /// it displays erros in the screen without running the commands in the textBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void syntaxButtonClick(object sender, EventArgs e)
        {
            errorBox.Text = "";
            parser.setLines(inputWindows.Text);//passes the string to be parsed
            parser.checkingSyntax();//check syntax of the textbox
            errorBox.Text = parser.getErrors();//sets the errorBox to a string of errors
            parser.resetErrors();
        }

        private void PressedEnterKeyCommandLine(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) //if enter key is pressed
            {
                errorBox.Text = "";
                parser.setLines(commandLine.Text); //passes the string to be parsed
                parser.parseCommandLine();//check for errors and output the graphics
                outputWindows.Refresh(); //refreshes the windows so the graphics are loaded in it
                errorBox.Text = parser.getErrors();//sets the errorBox to a string of errors
                parser.resetErrors();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
