﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{
    /// <summary>
    /// class that holds information about a circle
    /// </summary>
    class Triangle : Shape
    {
        int point2x, point2y, point3x, point3y; //they say where the other 2 points are

        /// <summary>
        /// constructor that inicialises the variables manually
        /// </summary>
        /// <param name="color"></param> color object of the fill and pen of the shape
        /// <param name="x"></param> x position in the coordinate
        /// <param name="y"></param> y position of the coordinate
        /// <param name="width"></param> width of the shape
        /// <param name="height"></param> height of the shape
        public Triangle(Color color, int x, int y, int point2x, int point2y, int point3x, int point3y ) : base(color, x, y)
        {
            this.point2x = point2x;
            this.point2y = point2y;
            this.point3x = point3x;
            this.point3y = point3y;
        }

        /// <summary>
        /// constructor that sets variable depending on parameters list
        /// </summary>
        /// <param name="colour"></param> color object
        /// <param name="list"></param> list of parameters
        public Triangle(Color colour, params int[] list) : base(colour, list[0], list[1])
        {
            this.colour = colour;
            this.point2x = list[2];
            this.point2y = list[3];
            this.point3x = list[4];
            this.point3y = list[5];
        }

        ///<inheritdoc/>
        public override void draw(Graphics g)
        {
            this.g = g;
            Pen p = new Pen(colour, 2);// creates a pen object with a color
            Point[] points = { new Point(x, y), new Point(point2x, point2y), new Point(point3x, point3y) };
            g.DrawPolygon(p, points); //draws a Triangle shape to the graphics
        }

        ///<inheritdoc cref="CommandInterface" />
        public override void fill()
        {
            SolidBrush brush = new SolidBrush(colour); //creates a brush object
            Point[] points2 = { new Point(x, y), new Point(point2x, point2y), new Point(point3x, point3y) };
            g.FillPolygon(brush, points2); //fills the Triangle
        }

        public override void set(Color colour, params int[] list)
        {
            base.set(colour, list[0], list[1]);
            this.point2x = list[2];
            this.point2y = list[3];
            this.point3x = list[4];
            this.point3y = list[5];
        }

        ///<inheritdoc/>
        public override string ToString()
        {
            return base.ToString() + "    " + this.point2x + "," + this.point2y + "," + this.point3x + "," + this.point3y + " : ";
        }
    }
}
