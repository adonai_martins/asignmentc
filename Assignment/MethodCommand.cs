﻿using System;
using System.Collections.Generic;

namespace Assignment
{
    internal class MethodCommand
    {
        private string[] lines; // has the lines of the input textbox in an array
        private string methodNameCall;
        private int lineAfterMethodCall = 0;

        Dictionary<string, Dictionary<string,dynamic>> methodList = new Dictionary<string, Dictionary<string, dynamic>>();//stores the method lines, parameters, etc
        Dictionary<string, string[]> methodCall = new Dictionary<string, string[]>();


        private bool insideMethodCall = false;

        private int countMethodCallLines = 0;

        /// <summary>
        /// constructor that initialises the lines
        /// </summary>
        /// <param name="lines"></param>//lines of the input textbox
        public MethodCommand(string[] lines)
        {
            this.lines = lines;
        }

        
        //method need to be split into 2
        /// <summary>
        /// method that stores the line inside a function into a dictionary and stores informations of the method parameter
        /// </summary>
        /// <param name="lineOffset"></param>
        /// <returns></returns>
        public int validateDeclarationHeader(int lineOffset)
        {
            lines[lineOffset] = lines[lineOffset].Trim().ToLower(); //splits lines into commands and parameters and puts them in "lines"

            if (lines[lineOffset].Contains("method ") && lines[lineOffset].Contains("(") && insideMethodCall == false) // if line contains "method" and  "("
            {
                Dictionary<string, dynamic> methodData = new Dictionary<string, dynamic>(); //stores the data of the method


                //split and store data in array
                string[] LineBits = lines[lineOffset].Trim().Split('('); //stores bits in lines
                string[] commands = LineBits[0].Trim().Split(' '); // stores the commands
                string[] parameters = LineBits[1].Trim().Split(','); // stores the parameters
                parameters[parameters.Length - 1] = parameters[parameters.Length - 1].Remove(parameters[parameters.Length - 1].Length - 1); //removes the ")" from the parameters

                methodData.Add("methodCodeLines", new List<string>());//add the methodlines key and assigns a value to a new list
                methodData.Add("parameters", parameters); //add the parameters key and assign a value to a new list 
                methodData.Add("indexStart", lineOffset + 1);
                string methodName = "" ; //name of the method
                methodName = commands[1];


                methodList.Add(methodName, methodData); // adds the function the the list of functions

                int count = lineOffset + 1; //we move to the next line
                string lineToAdd = "";

                //while loop that adds the lines of the method to a list

                while (!(lineToAdd.Trim().Contains("endmethod") && lineToAdd.Length == 9)) //if the line does not contains "endmethod" and the length is 9, stop adding lines turn declaration status to false
                {
                    lineToAdd = lines[count].Trim().ToLower(); //stores a line to be added in the method lines

                    if (lineToAdd.Length>1 && !lineToAdd.Trim().Contains("endmethod"))//if the length is larger than 1 and lineToAdd does not contain "endMethod"
                    {
                        methodList[methodName]["methodCodeLines"].Add(lineToAdd);//adds the line to the list

                    }
                    else if(lineToAdd.Trim().Contains("endmethod")) //if line does not contain "endMethod"
                    {
                        break;
                    }
                    count++;//cound the lines inside the method declaration
                    lineOffset = count; //add 1 to line offset
                }
                methodData.Add("indexEnd", count + -1);

                lineOffset = count + 1;
            }//enf of if

            return lineOffset;
        }//end of method

        /// <summary>
        /// method that validates when a method is called
        /// </summary>
        /// <param name="lineOffset"></param>
        /// <returns></returns>
        public int validateMethodCall(int lineOffset)
        {
            string line = lines[lineOffset].Trim().ToLower();

            if (line.Contains("(") && line.Contains(")") ) //if line contains "(" and ")"
            {
                if (!(line.Contains("method "))) //if line does not contain the word method
                {
                    string[] test = lines[lineOffset].Trim().Split(')');

                    //Console.WriteLine(test.Length);

                    lineAfterMethodCall = lineOffset + 1;

                    string[] LineBits = lines[lineOffset].Trim().Split('('); //stores bits in lines
                    string[] parameters = LineBits[1].Trim().Split(','); // stores the parameters
                    parameters[parameters.Length - 1] = parameters[parameters.Length - 1].Remove(parameters[parameters.Length - 1].Length - 1); //removes the ")" from the parameters

                    string methodName = LineBits[0]; //name of the method

                    methodNameCall = methodName;
                    methodCall.Add("parameters", parameters);


                    countMethodCallLines = methodList[methodName]["methodCodeLines"].Count + 1;

                    insideMethodCall = true;

                    return methodList[methodName]["indexStart"];
                }
            }//end if
            return lineOffset;
            
        }//end method


        //method needs to be split into 2 methods
        /// <summary>
        /// method that resets the lines to where they we
        /// </summary>
        /// <param name="lineOffset"></param>
        /// <returns></returns>
        public int resetMethodLines(int lineOffset)
        {
            //Console.WriteLine(lines[lineOffset]);
            //replace the values of thewith parameters
            if (insideMethodCall == true && lines[lineOffset].Length > 0 && countMethodCallLines > 0) //if we are inside the method when we call it
            {
                int i = 0;

              //Console.WriteLine(lines[lineOffset]); //receives it wrongly here

                foreach (string parameterName in methodList[methodNameCall]["parameters"])
                {
                    string parameterValue = methodCall["parameters"][i];

                    // Console.WriteLine(parameterName + "   " + parameterValue);


                    lines[lineOffset] = lines[lineOffset].Replace(parameterName, parameterValue);//replace values of the function
                                                                                                 // Console.WriteLine(lines[lineOffset]);
                    //Console.WriteLine(lines[lineOffset]);


                    i = i +1;
                }

                countMethodCallLines = countMethodCallLines - 1;
                if (lines[lineOffset].Trim().ToLower().Contains("endif"))
                {
                    countMethodCallLines = countMethodCallLines - 2;
                }
            }

            //replaces the lines with the method call parameters with the default name for another method to use replace them again
            if (insideMethodCall == true && countMethodCallLines == 0)
            {//methodNameCall
             //replace all the lines from method line start with the lines using a foreach loop
                insideMethodCall = false;
                //return the line after the method call "method(1,2,3,4)
                //   Console.WriteLine("size" + methodList[methodNameCall]["parameters"].Length + " length " + methodList[methodNameCall]["parameters"].Length);


                int startOfMethodCall = methodList[methodNameCall]["indexStart"];
                int endOfMethodCall = methodList[methodNameCall]["indexEnd"];
  //              Console.WriteLine(lines[lineOffset] + " index " + endOfMethodCall + " end / start " + startOfMethodCall);
//                 Console.WriteLine(lines[lineOffset] + " index " + endOfMethodCall + " end / start " + startOfMethodCall);
                int methodCodeLineCounter = 0;

                for (int index = startOfMethodCall; index < endOfMethodCall; index++) // starts at line inside the method, ends with the count of lines plus start of lines
                {
                  //   Console.WriteLine(index);

                    //       Console.WriteLine(lines[lineOffset] + " index " + endOfMethodCall);

                    if (lines[index].Length > 0)
                    {
                        lines[index] = methodList[methodNameCall]["methodCodeLines"][methodCodeLineCounter];
                        methodCodeLineCounter++;
                    }

                   // Console.WriteLine(lines[lineOffset]);


                }
                methodCall.Clear();
                return lineAfterMethodCall;
            }
            return lineOffset;
        }//end of method
    }//enf of Class
}//end of namespace