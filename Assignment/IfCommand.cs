﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{
    /// <summary>
    /// class that validates the if commands
    /// </summary>
    class IfCommand
    {

        string[] lines;//stores the array of lines
        bool operationResult = true;//stores where he if condition is true or false
        int endIfCound = 0;
        int ifCount = 0;




        /// <summary>
        /// constructor that takes an array of lines
        /// </summary>
        /// <param name="lines"></param> //array of lines
        public IfCommand(string[] lines)
        {
            this.lines = lines;
        }

        /// <summary>
        /// method that validates if statements in one line
        /// </summary>
        /// <param name="lineOffset"></param> the line offset of the array to be validated
        public void validateIfStatementOneLine(int lineOffset)
        {

            lines[lineOffset] = lines[lineOffset].Trim().ToLower(); //splits lines into commands and parameters and puts them in "lines"
            string[] LineBits = lines[lineOffset].Split(' '); //stores bits in lines

            if (LineBits[0].Equals("if") && LineBits.Length > 2)
            {
                string ifOperation = LineBits[1].ToUpper().Trim(); //stores the ifOperation
                
                if (ifOperation.Contains("=="))
                {
                    int[] ifValues = ifValuesToInteger(ifOperation, "=="); //changes the values to integers

                    if (ifValues[0] == ifValues[1]) //if the left of the operation equals the right value
                    {
                        int removeToChar = findNthCharPosition(lines[lineOffset], ' ', 2) + 1; //find the third space char and adds 1
                        lines[lineOffset] = lines[lineOffset].Trim().Remove(0, removeToChar); //removes 
                    }
                    else
                    {
                        lines[lineOffset] = "";
                    }
                }
                else if (ifOperation.Contains("!="))
                {
                    int[] ifValues = ifValuesToInteger(ifOperation, "!="); //changes the values to integers

                    if (ifValues[0] != ifValues[1]) //if the left of the operation equals the right value
                    {
                        int removeToChar = findNthCharPosition(lines[lineOffset], ' ', 2) + 1; //find the third space char and adds 1
                        lines[lineOffset] = lines[lineOffset].Remove(0, removeToChar); //removes 
                    }
                    else
                    {
                        lines[lineOffset] = "";
                    }
                }
                else if (ifOperation.Contains(">") && !(ifOperation.Contains("=")))
                {
                    int[] ifValues = ifValuesToInteger(ifOperation, ">"); //changes the values to integers

                    if (ifValues[0] > ifValues[1]) //if the left of the operation equals the right value
                    {
                        int removeToChar = findNthCharPosition(lines[lineOffset], ' ', 2) + 1; //find the third space char and adds 1
                        lines[lineOffset] = lines[lineOffset].Remove(0, removeToChar); //removes 
                    }
                    else
                    {
                        lines[lineOffset] = "";
                    }
                }
                else if (ifOperation.Contains("<") && !(ifOperation.Contains("=")))
                {
                 //   Console.WriteLine("aqui");

                    int[] ifValues = ifValuesToInteger(ifOperation, "<"); //changes the values to integers

                    if (ifValues[0] < ifValues[1]) //if the left of the operation equals the right value
                    {
                        int removeToChar = findNthCharPosition(lines[lineOffset], ' ', 2) + 1; //find the third space char and adds 1
                        lines[lineOffset] = lines[lineOffset].Remove(0, removeToChar); //removes 
                    }
                    else
                    {
                        lines[lineOffset] = "";
                    }
                }
                else if (ifOperation.Contains(">="))
                {
                    int[] ifValues = ifValuesToInteger(ifOperation, ">="); //changes the values to integers

                    if (ifValues[0] >= ifValues[1]) //if the left of the operation equals the right value
                    {
                        int removeToChar = findNthCharPosition(lines[lineOffset], ' ', 2) + 1; //find the third space char and adds 1
                        lines[lineOffset] = lines[lineOffset].Remove(0, removeToChar); //removes 
                    }
                    else
                    {
                        lines[lineOffset] = "";
                    }
                }
                else if (ifOperation.Contains("<="))
                {
                    int[] ifValues = ifValuesToInteger(ifOperation, "<="); //changes the values to integers

                    if (ifValues[0] <= ifValues[1]) //if the left of the operation equals the right value
                    {
                        int removeToChar = findNthCharPosition(lines[lineOffset], ' ', 2) + 1; //find the third space char and adds 1
                        lines[lineOffset] = lines[lineOffset].Remove(0, removeToChar); //removes 
                    }
                    else
                    {
                        lines[lineOffset] = "";
                    }
                }else
                {
                    throw new ArgumentNullException("Wrong Sign operator");
                }
            }
        }

        /// <summary>
        /// find the nth position of a charater
        /// </summary>
        /// <param name="theString"></param> string to be scanned
        /// <param name="charToBeFound"></param> char to be found
        /// <param name="nthOcurrence"></param> nth ocurrence
        /// <returns></returns> the position of the nth ocurrence
        private int findNthCharPosition(string theString, char charToBeFound, int nthOcurrence)
        {
            int count = 0;
            for (int index = 0; index < theString.Length; index++)
            {
                if (theString[index] == charToBeFound)
                {
                    count++;
                    if (count == nthOcurrence)
                    {
                        return index;
                    }
                }
            }
            return -1;
        }



        /// <summary>
        /// method that converts the values of the if operation to integer and returns an array
        /// </summary>
        /// <param name="conditionToCompare"></param> condition to compare string
        /// <param name="delimiter"></param> //the delimiter that separates the values
        /// <returns></returns> returns an array of the values as integer
        private int[] ifValuesToInteger(string conditionToCompare, string delimiter)
        {
            int[] comparableValues = new int[2]; // stores the values to be compared

            string[] separators = new string[] { delimiter }; //separates the lines by new line and returns chars
            string[] comparableValuesString = conditionToCompare.Split(separators, StringSplitOptions.None); //splits string into lines and store them in array

            comparableValues[0] = Int32.Parse(comparableValuesString[0]); // transform the values of the comparsion to be executed to integer
            comparableValues[1] = Int32.Parse(comparableValuesString[1]); // transform the values of the comparsion to be executed to integer

            return comparableValues;
        }



        /// <summary>
        /// this method validates the if statement
        /// </summary>
        /// <param name="lineOffset"></param>
        public int validateIfStatement(int lineOffset)
        {


            lines[lineOffset] = lines[lineOffset].Trim().ToLower(); //splits lines into commands and parameters and puts them in "lines"
            string[] LineBits = lines[lineOffset].Trim().Split(' '); //stores bits in lines

            if (LineBits[0].Equals("if"))
            {
                string ifOperation = LineBits[1].ToUpper().Trim(); //stores the ifOperation
                ifCount = ifCount + 1;
                if (ifOperation.Contains("=="))
                {
                    int[] ifValues = ifValuesToInteger(ifOperation, "=="); //changes the values to integers

                    if (ifValues[0] == ifValues[1]) //if the left of the operation equals the right value
                    {
                        operationResult = true;
                    }
                    else
                    {
                        operationResult = false;
                    }
                }
                else if (ifOperation.Contains("!="))
                {
                    int[] ifValues = ifValuesToInteger(ifOperation, "!="); //changes the values to integers

                    if (ifValues[0] != ifValues[1]) //if the left of the operation equals the right value
                    {
                        operationResult = true;
                    }
                    else
                    {
                        operationResult = false;
                    }
                }
                else if (ifOperation.Contains(">") && !(ifOperation.Contains("=")))
                {
                    int[] ifValues = ifValuesToInteger(ifOperation, ">"); //changes the values to integers

                    if (ifValues[0] > ifValues[1]) //if the left of the operation equals the right value
                    {
                        operationResult = true;
                    }
                    else
                    {
                        operationResult = false;
                    }
                }
                else if (ifOperation.Contains("<") && !(ifOperation.Contains("=")))
                {
                    int[] ifValues = ifValuesToInteger(ifOperation, "<"); //changes the values to integers

                    if (ifValues[0] < ifValues[1]) //if the left of the operation equals the right value
                    {
                        operationResult = true;
                    }
                    else
                    {
                        operationResult = false;
                    }
                }
                else if (ifOperation.Contains(">="))
                {
                    int[] ifValues = ifValuesToInteger(ifOperation, ">="); //changes the values to integers

                    if (ifValues[0] >= ifValues[1]) //if the left of the operation equals the right value
                    {
                        operationResult = true;
                    }
                    else
                    {
                        operationResult = false;
                    }
                }
                else if (ifOperation.Contains("<="))
                {
                    int[] ifValues = ifValuesToInteger(ifOperation, "<="); //changes the values to integers

                    if (ifValues[0] <= ifValues[1]) //if the left of the operation equals the right value
                    {
                        operationResult = true;
                    }
                    else
                    {
                        operationResult = false;
                    }
                }
                else
                {
                    throw new ArgumentNullException("Wrong Sign operator");
                }
                lineOffset = lineOffset + 1;
            }
            return lineOffset;
        }

        public int validateEndIf(int lineOffset)
        {
            string line = lines[lineOffset].Trim().ToLower(); //splits lines into commands and parameters and puts them in "lines"

            if (operationResult == false && !(line.Contains("endif") && line.Length == 5)) //if false and the line does not contain elseif, delete what it is in the line
            {
                lines[lineOffset] = "";
            }
            if (line.Contains("endif") && line.Trim().Length == 5 ) //if the word endif found and the length is 5
            {
                endIfCound = endIfCound +1;
                operationResult = true;
                lineOffset = lineOffset + 1;
            }
            Console.WriteLine(ifCount);
            if (lineOffset == lines.Length -1 && ifCount != endIfCound)
            {
                throw new ArgumentNullException("missing endif");
                //throw error
            }

            return lineOffset;
        }
    }
}
