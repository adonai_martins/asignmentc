﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{
    /// <summary>
    /// class that holds information about a circle
    /// </summary>
    class Circle : Shape
    {
        int width, height; //radius of the Cirgle

        /// <summary>
        /// constructor that inicialises the variables
        /// </summary>
        /// <param name="color"></param> color object of the fill and pen of the shape
        /// <param name="x"></param> x position in the coordinate
        /// <param name="y"></param> y position of the coordinate
        /// <param name="width"></param> width of the shape
        /// <param name="height"></param> height of the shape
        public Circle(Color color, int x, int y, int radius) : base(color, x, y)
        {
            width = radius * 2;
            height = radius * 2;
        }

        /// <summary>
        /// constructor that sets variable depending on parameters
        /// </summary>
        /// <param name="colour"></param> color object
        /// <param name="list"></param> list of parameters
        public Circle(Color colour, params int[] list) : base(colour, list[0], list[1])
        {
            this.colour = colour; 
            width = list[2] * 2;
            height = list[2] * 2;

        }

        ///<inheritdoc cref="Shape" />
        public override void draw(Graphics g)
        {
            this.g = g;
            Pen p = new Pen(colour, 2);// creates a pen object with a color
            g.DrawEllipse(p, x, y, width, height); //draws a Circle shape to the graphics
        }

        ///<inheritdoc cref="CommandInterface" />
        public override void fill()
        {
            SolidBrush brush = new SolidBrush(colour); //creates a brush object
            g.FillEllipse(brush, x, y, width, height); //fills the Circle
        }

        ///<inheritdoc/>
        public override void set(Color colour, params int[] list)
        {
            base.set(colour, list[0], list[1]);
            width = list[2] * 2;
            height = list[2] * 2;
        }

        ///<inheritdoc/>
        public override string ToString()
        {
            return base.ToString() + "    " + this.width/2 + " : ";
        }
    }
}
