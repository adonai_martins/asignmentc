﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{

    class WrongColorException : Exception
    {
        string Parameter;
        public WrongColorException(string Parameter)
        {
            this.Parameter = Parameter;

        }
        public override string Message
        {
            get
            {
                return "Parameter" + Parameter + "should be a color(Yellow, Blue, Red, Green, Black)";
            }
        }
    }
}
