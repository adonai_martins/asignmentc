﻿using System;

namespace Assignment
{
    internal class ForCommand
    {
        private string[] lines;
        private int loopCounter = 0;
        private int loopStartingLine = 0;


        /// <summary>
        /// constructore that 
        /// </summary>
        /// <param name="lines"></param>
        public ForCommand(string[] lines)
        {
            this.lines = lines;
        }

        /// <summary>
        /// method that validates the forLoop Header
        /// </summary>
        /// <param name="lineOffset"></param>
        /// <returns></returns>
        public int validateForLoop(int lineOffset)
        {
            lines[lineOffset] = lines[lineOffset].Trim().ToLower(); //splits lines into commands and parameters and puts them in "lines"
            string[] LineBits = lines[lineOffset].Split(' '); //stores bits in lines

            if (LineBits[0].Equals("loop") && LineBits[1].Equals("for")) // if the first word is loop and the second word is for
            {
                loopStartingLine = lineOffset; //the starting line where the loop starts is equal to the line offset
                loopCounter = Int32.Parse(LineBits[2]); //store the ammount of turns to be loop
                return 1; //returns one to move the index one line ahead
            }
            else
            {
                return 0; //returns 0
            }
        }//end method


        /// <summary>
        /// method that validates endFor keyword. if found but loop counter it returns the line offset where the loop starts
        /// </summary>
        /// <param name="lineOffset"></param>//returns the line offset where the loop should start, or the index of the same index so the programs carries on
        /// <returns></returns>
        public int validateEndFor(int lineOffset)
        {
            string line = lines[lineOffset].Trim().ToLower(); //splits lines into commands and parameters and puts them in "lines"
            
            if (line.Contains("endfor") && line.Trim().Length == 6 && loopCounter > 1) //if the line contains "endfor" and the length is 6 and loop counter is larger than 1
            {
                lineOffset = loopStartingLine;
                loopCounter = loopCounter - 1;
            }
            if (line.Contains("endfor") && line.Trim().Length == 6 && loopCounter == 1) //if the line contains "endfor" and the length is 5 loop counter is equal than 1
            {
                lineOffset = lineOffset + 1;
            }
            return lineOffset;

        }//end method
    }//end class
}//end namespace