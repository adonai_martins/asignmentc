﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{
    class MoveToCommand
    {
        
        public void CheckCommand(string command, Canvas canvas, string error, string [] individualParameters)
        {
            if ("MOVETO".Equals(command))
            {
                try
                {   //convert parameters strings to integerss
                    int parameter1 = Int32.Parse(individualParameters[0]);//
                    int parameter2 = Int32.Parse(individualParameters[1]);//
                    canvas.moveTo(parameter1, parameter2); //move cursor
                }
                catch (Exception e)
                {
                    //errors = "parameter must be a number or wrong paramer";//updates the error variable
                    canvas.clearOutputWindow(); //clears the outputwindows
                }
            }
        }
    }
}
