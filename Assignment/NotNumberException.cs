﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{
    class NotNumberException : Exception
    {

        //                parameter must be a number or wrong parameter");
        public override string Message
        {
            get
            {
                return "parameter must be a number or wrong parameter";
            }
        }
    }
}
