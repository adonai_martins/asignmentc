﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{

    /// <summary>
    /// Class that checks for varaible in an array, strores it and replaces it with the values the variable is
    /// </summary>
    class VarComman
    {

        Dictionary<string, string> variables = new Dictionary<string, string>();//stores the variables
        Dictionary<int, string> variableLines = new Dictionary<int, string>();//stores the variables

        string[] lines;//stores the array of lines


        /// <summary>
        /// constructor that stores all the lines
        /// </summary>
        /// <param name="lines"></param> it takes an array of lines as a param
        public VarComman(string[] lines)
        {
            this.lines = lines;
        }

        /// <summary>
        /// method that replaces the variable names with values
        /// </summary>
        /// <param name="lineOffset"></param>//it requires the offset of the line to be validated
        public void replaceVariablesWithValues(int lineOffset)
        {
            string line = lines[lineOffset].ToLower().Trim();
            if (line.Length > 3 
                && !(line.Contains("clear")) 
                && !(line.Contains("reset")) 
                && !(line.Contains("endmethod")) 
                && !(line.Contains("endif")) 
                && !(line.Contains("endfor"))
                )//if the line has a length larger than 3 and doesn't contain "clear", "reset", "endmethod", "endif", "endfor"
            {// 
                line = lines[lineOffset].Trim().ToLower().Substring(0, 6);// we grab the first 6 chars and see if those are not the word "method"
                if (!(line.Contains("method")))
                {
                    foreach (string key in variables.Keys)//loops through array keys, and loop for key names on the line and replaces it with the value
                    {
                        if (lines[lineOffset].Contains("=") && !(lines[lineOffset].ToLower().Contains("if")))//if line contains "=" and does not have "if"
                        {
                            string a;
                            string b;
                            //Console.WriteLine(lines[lineOffset].Substring(0, lines[lineOffset].IndexOf('=') +2));
                            if (!variableLines.ContainsKey(lineOffset))
                            {
                                variableLines.Add(lineOffset, lines[lineOffset]);
                                a = lines[lineOffset].Substring(0, lines[lineOffset].IndexOf('=') + 2);
                                b = lines[lineOffset].Remove(0, a.Length);
                            }
                            else
                            {
                                a = variableLines[lineOffset].Substring(0, lines[lineOffset].IndexOf('=') + 2);
                                b = variableLines[lineOffset].Remove(0, a.Length);
                            }



                           // Console.WriteLine(a + "|||||||||||||||" +  b);

                            b = b.Replace(key, variables[key]);

                                                            
                            lines[lineOffset] = a + b;


                        }
                        else
                        {
                            Console.WriteLine(lines[lineOffset]);

                            
                            if (!variableLines.ContainsKey(lineOffset))
                            {
                                variableLines.Add(lineOffset, lines[lineOffset]);

                                lines[lineOffset] = lines[lineOffset].Replace(key, variables[key]);

                            }
                            else
                            {

                                lines[lineOffset] = variableLines[lineOffset].Replace(key, variables[key]);

                            }



                        }

                    }
                }
            }
        }//END METHOD


        /// <summary>
        /// method that stores the variable in a list and deletes the line once the operation is done
        /// </summary>
        /// <param name="lineOffset"></param> it takes the line offset to be scanned
        public void storeVariableInList (int lineOffset)
        {
            string[] LineBits = lines[lineOffset].Trim().Split('='); //cleans the lines. and slipts it

            if (LineBits.Length == 2 && !(lines[lineOffset].Contains("if")))//if there are three strings in the line/////////////////////////////
            {
                    try
                    {
                        string[] valuesAddition = LineBits[1].Trim().Split('+'); //array that splits by "+"
                        int result = 0; //keeps the operation result

                    for (int i = 0; i < valuesAddition.Length; i++)//goes through all the values and adds them
                        {
                            if (variables.ContainsKey(valuesAddition[i].Trim()))//if variable dictionary contains the linebit (a variable name). we search the word in the dictionary and if found, it replaces it with the value
                            {

                                string keyChecker = valuesAddition[i].Trim(); //store the value

                              //  Console.WriteLine();
                                valuesAddition[i] = variables[keyChecker].Trim();  //we search the word in the dictionary and if found, it replaces it with the value
                            }
                            result = result + Int32.Parse(valuesAddition[i].Trim()); //convert to int
                            Int32.Parse(valuesAddition[i]);//checks whether it is a number
                        }


                        string key = LineBits[0].Trim();//stores the key of the dictionary
                        string varValue = Convert.ToString(result);//stores the value of the variable


                        if (variables.ContainsKey(key))//if the variable already exists it assigns the value
                        {

                              variables[key] = varValue.Trim();  //assigns dictionary key with a new value


                    }
                    else // if not, it creates a new variable
                        {
                            variables.Add(key, varValue.Trim()); //add variable to dictionary
                        }
                    }
                    catch (Exception e)
                    {
                        throw new ArgumentNullException("Value Cannot be assigned to var");
                    }
            }
        }//END METHOD
    }//end class
}//end namespace
