﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{
    class DrawTo : Shape
    {

        int toX, toY;

        /// <summary>
        /// constructor that inicialises the variables
        /// </summary>
        /// <param name="color"></param> color object of the fill and pen of the shape
        /// <param name="x"></param> x position in the coordinate
        /// <param name="y"></param> y position of the coordinate

        public DrawTo(Color color, int[] list) : base(color, list[0], list[1])
        {
            toX = list[2];
            toY = list[3];
        }


        public override void draw(Graphics g)
        {
            this.g = g;
            Pen p = new Pen(colour, 2);// creates a pen object with a color
            g.DrawLine(p, x, x, toX, toY);
        }

    }
}
