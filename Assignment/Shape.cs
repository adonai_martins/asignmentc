﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{
	/// <summary>
	/// class that has basic information about shapes
	/// </summary>
	public abstract class Shape
    {
		protected Color colour; // color of the pen shape and color of the fill
		protected int x, y;  //x position in the coordinates 
		protected Graphics g;//graphics of the circle

		/// <summary>
		///	constructor that initialises variables equal to parameters
		/// </summary>
		/// <param name="color"></param> tales a color object
		/// <param name="x"></param> takes the x coordinates
		/// <param name="y"></param> takes the y coordinates
		public Shape(Color color, int x, int y) //with parameters
        {
			this.colour = color;
			this.x = x;
			this.y = y;

		}

        /// <summary>
        /// method that draws the actual shape
        /// </summary>
        /// <param name="g"></param> receives graphics object to creat a shape
        public abstract void draw(Graphics g);

		/// <summary>
		/// method that filles the shape with the same color of the Pen
		/// </summary>
		/// <param name="fillOnOff"></param> it takes a boolean. if true, it fills the shape, if false, it removes the color leaving the outline
		public virtual void fill() {
		
		}

		/// <summary>
		/// toString method that prints the variables
		/// </summary>
		/// <returns></returns> returns a string of the variables printed
        public override string ToString()
        {
			return base.ToString() + "    " + this.x + "," + this.y + " : ";
		}

		/// <summary>
		/// set properties of the shape. can be overiden by the children
		/// </summary>
		/// <param name="colour"></param> color of the shape
		/// <param name="list"></param> aditional parameteres
		public virtual void set(Color colour, params int[] list)
		{
			this.colour = colour;
			this.x = list[0];
			this.y = list[1];
		}
	}
}
