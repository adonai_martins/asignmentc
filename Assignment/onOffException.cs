﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{
    [Serializable()]
    class OnOffException : Exception
    {

        string parameters;
        public OnOffException(string parameters)
        {
            this.parameters = parameters;

        }
        public override string Message
        {
            get
            {
                return "parameter < " + parameters + ">  must be on/off";
            }
        }
    

    }


}
