﻿namespace Assignment
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.runButton = new System.Windows.Forms.Button();
            this.syntaxButton = new System.Windows.Forms.Button();
            this.inputWindows = new System.Windows.Forms.TextBox();
            this.commandLine = new System.Windows.Forms.TextBox();
            this.funnyImage = new System.Windows.Forms.PictureBox();
            this.outputWindows = new System.Windows.Forms.PictureBox();
            this.errorBox = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.funnyImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.outputWindows)).BeginInit();
            this.SuspendLayout();
            // 
            // runButton
            // 
            this.runButton.Location = new System.Drawing.Point(66, 330);
            this.runButton.Name = "runButton";
            this.runButton.Size = new System.Drawing.Size(75, 23);
            this.runButton.TabIndex = 0;
            this.runButton.Text = "Run";
            this.runButton.UseVisualStyleBackColor = true;
            this.runButton.Click += new System.EventHandler(this.runButton_Click);
            // 
            // syntaxButton
            // 
            this.syntaxButton.Location = new System.Drawing.Point(173, 330);
            this.syntaxButton.Name = "syntaxButton";
            this.syntaxButton.Size = new System.Drawing.Size(85, 23);
            this.syntaxButton.TabIndex = 1;
            this.syntaxButton.Text = "Syntax";
            this.syntaxButton.UseVisualStyleBackColor = true;
            this.syntaxButton.Click += new System.EventHandler(this.syntaxButtonClick);
            // 
            // inputWindows
            // 
            this.inputWindows.Location = new System.Drawing.Point(66, 37);
            this.inputWindows.Multiline = true;
            this.inputWindows.Name = "inputWindows";
            this.inputWindows.Size = new System.Drawing.Size(554, 230);
            this.inputWindows.TabIndex = 2;
            // 
            // commandLine
            // 
            this.commandLine.Location = new System.Drawing.Point(66, 282);
            this.commandLine.Name = "commandLine";
            this.commandLine.Size = new System.Drawing.Size(554, 22);
            this.commandLine.TabIndex = 3;
            this.commandLine.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PressedEnterKeyCommandLine);
            // 
            // funnyImage
            // 
            this.funnyImage.Image = global::Assignment.Properties.Resources.funnydog;
            this.funnyImage.Location = new System.Drawing.Point(395, 319);
            this.funnyImage.Name = "funnyImage";
            this.funnyImage.Size = new System.Drawing.Size(225, 161);
            this.funnyImage.TabIndex = 5;
            this.funnyImage.TabStop = false;
            // 
            // outputWindows
            // 
            this.outputWindows.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.outputWindows.Location = new System.Drawing.Point(674, 37);
            this.outputWindows.Name = "outputWindows";
            this.outputWindows.Size = new System.Drawing.Size(527, 434);
            this.outputWindows.TabIndex = 4;
            this.outputWindows.TabStop = false;
            this.outputWindows.Paint += new System.Windows.Forms.PaintEventHandler(this.drawOutput);
            // 
            // errorBox
            // 
            this.errorBox.Location = new System.Drawing.Point(76, 375);
            this.errorBox.Name = "errorBox";
            this.errorBox.ReadOnly = true;
            this.errorBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.errorBox.Size = new System.Drawing.Size(283, 96);
            this.errorBox.TabIndex = 6;
            this.errorBox.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1253, 501);
            this.Controls.Add(this.errorBox);
            this.Controls.Add(this.funnyImage);
            this.Controls.Add(this.outputWindows);
            this.Controls.Add(this.commandLine);
            this.Controls.Add(this.inputWindows);
            this.Controls.Add(this.syntaxButton);
            this.Controls.Add(this.runButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.funnyImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.outputWindows)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button runButton;
        private System.Windows.Forms.Button syntaxButton;
        private System.Windows.Forms.TextBox inputWindows;
        private System.Windows.Forms.TextBox commandLine;
        private System.Windows.Forms.PictureBox outputWindows;
        private System.Windows.Forms.PictureBox funnyImage;
        private System.Windows.Forms.RichTextBox errorBox;
    }
}

