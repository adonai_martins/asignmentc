﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{   
    /// <summary>
    /// class factory that returns a new object of the specific shape
    /// </summary>
    public class ShapeFactory
    {
        /// <summary>
        /// method that returns an object of the specific shape
        /// </summary>
        /// <param name="shapeType"></param> says what type of shape we want
        /// <param name="colour"></param> color of the shape
        /// <param name="list"></param> list of parameters
        /// <returns></returns>returns a shape object
        /// <exception> throws ArgumentException "shape does not exist</exception>
        public Shape getShape(String shapeType, Color colour, params int[] list)
		{

            shapeType = shapeType.ToUpper().Trim(); //trims the shapetype parameter and converts it into upperCase


            //returns shape object depending on the string input
            if (shapeType.Equals("RECTANGLE"))
            {
                Shape a = new Rectangle(colour, list);
                
                return new Rectangle(colour, list);

            }
            else if (shapeType.Equals("CIRCLE"))
            {
                return new Circle(colour, list);

            }
            else if (shapeType.Equals("TRIANGLE"))
            {
                return new Triangle(colour, list);
                
            }
            else if (shapeType.Equals("DRAWTO"))
            {


                return new DrawTo(colour, list);

            }
            else
            {
                //throws exception in case there is no shape
                System.ArgumentException argEx = new System.ArgumentException("Factory error: " + shapeType + " does not exist");
                throw argEx;
            }
        }
    }
}
