﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{
    /// <summary>
    /// class tha draws rectangles
    /// </summary>
    class Rectangle : Shape
    {
        int width, height; //width and height of the same

        /// <summary>
        /// constructor that inicialises the variables manually
        /// </summary>
        /// <param name="color"></param> color object of the fill and pen of the shape
        /// <param name="x"></param> x position in the coordinate
        /// <param name="y"></param> y position of the coordinate
        /// <param name="width"></param> width of the shape
        /// <param name="height"></param> height of the shape
        public Rectangle(Color color, int x, int y, int width, int height): base(color, x, y)
        {
            this.width = width;
            this.height = height;
        }

        /// <summary>
        /// constructor that sets variable depending on parameters list
        /// </summary>
        /// <param name="colour"></param> color object
        /// <param name="list"></param> list of parameters
        public Rectangle(Color colour, params int[] list) : base(colour, list[0], list[1])
        {
            this.colour = colour;
            this.width = list[2];
            this.height = list[3];
        }

        ///<inheritdoc cref="Shape" />
        public override void draw(Graphics g)
        {
            this.g = g;
            Pen p = new Pen(colour, 2);// creates a pen object with a color
            g.DrawRectangle(p, x, y, width, height); //draws a rectangle shape to the graphics
        }

        ///<inheritdoc cref="CommandInterface" />
        public override void fill()
        {
                SolidBrush brush = new SolidBrush(colour); //creates a brush object
                g.FillRectangle(brush, x, y, width, height); //fills the rectangle
        }

        ///<inheritdoc cref="Shape" />
        public override void set(Color colour, params int[] list)
        {
            base.set(colour, list[0], list[1]);
            this.width = list[2];
            this.height = list[3];
        }

        ///<inheritdoc cref="Shape" />
        public override string ToString()
        {
            return base.ToString() + "    " + this.width + "," + this.height + " : ";
        }

    }
}
