﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace Assignment
{
    /// <summary>
    /// class that holds the information about the strings to be checked the syntax
    /// </summary>
    class Parser
    {
        private string[] lines;//stores all the String lines
        Canvas canvas; //object of class canvas to draw in the screen
        String errors;
        /// <summary>
        /// constructor to inicialise canvas
        /// </summary>
        /// <param name="canvas"></param> takes a canva object that draws graphics
        public Parser(Canvas canvas)
        {
            this.canvas = canvas;
        }

        /// <summary>
        /// constructor to iniciallise variables
        /// </summary>
        /// <param name="inputString"></param> takes a string of lines
        /// <param name="canvas"></param> takes a canvas object that draws graphics
        public Parser(string inputString, Canvas canvas)
        {
            this.canvas = canvas;
            string[] separators = new string[] { "\r\n"}; //separates the lines by new line and returns chars
            lines = inputString.Split(separators, StringSplitOptions.None); //splits string into lines and store them in array
        }





        /// <summary>
        /// method that checks for errors and draw all the graphics
        /// </summary>
        public void parse()
        {
          // Dictionary<string, string> variables = new Dictionary<string, string>();//a dictionary object to store the variables
            VarComman varCommand = new VarComman(lines);
            IfCommand ifCommand = new IfCommand(lines);
            ForCommand forCommand = new ForCommand(lines);
            MethodCommand methodCommand = new MethodCommand(lines);
            CommandsCheck comandsChecker = new CommandsCheck(lines, canvas);

            canvas.reset(); //clears the outputwindows

            for (int index = 0; index < lines.Length ; index++) //scans through lines of the textbox
            {
                try
                {
                    index = methodCommand.validateDeclarationHeader(index);
                    index = methodCommand.validateMethodCall(index);
                    index = methodCommand.resetMethodLines(index);

                    index = forCommand.validateForLoop(index) + index;
                    index = forCommand.validateEndFor(index);

                    varCommand.replaceVariablesWithValues(index);
                    varCommand.storeVariableInList(index);

                    ifCommand.validateIfStatementOneLine(index);
                    index = ifCommand.validateIfStatement(index);
                    index = ifCommand.validateEndIf(index);

                    comandsChecker.validateCommandsOfLine(index);

                }
                catch (Exception e)
                {
                    if (!(e.Message.Equals("Index was outside the bounds of the array.")))
                    {
                        errors = errors + "Line " + index + ": " + e.Message;

                        canvas.clearOutputWindow(); //clears the outputwindows
                        break;
                    }
                }
            }//end of for
        }//end of method


        public void parseCommandLine()
        {
            CommandsCheck comandsChecker = new CommandsCheck(lines, canvas);

            for (int index = 0; index < lines.Length; index++) //scans through lines of the textbox
            {
                try
                {
                    comandsChecker.validateCommandsOfLine(index);
                }
                catch (Exception e)
                {
                    if (!(e.Message.Equals("Index was outside the bounds of the array.")))
                    {
                        errors = e.Message;

                        canvas.clearOutputWindow(); //clears the outputwindows
                        break;
                    }
                }
            }
        }


        /// <summary>
        /// method that checks the syntax without running the code
        /// </summary>
        public void checkingSyntax() 
        {

            VarComman varCommand = new VarComman(lines);
            IfCommand ifCommand = new IfCommand(lines);
            ForCommand forCommand = new ForCommand(lines);
            MethodCommand methodCommand = new MethodCommand(lines);
            CommandsCheck comandsChecker = new CommandsCheck(lines, canvas);

            for (int index = 0; index < lines.Length; index++) //scans through lines of the textbox
            {
                try
                {
                    index = methodCommand.validateDeclarationHeader(index);
                    index = methodCommand.validateMethodCall(index);
                    index = methodCommand.resetMethodLines(index);

                    index = forCommand.validateForLoop(index) + index;
                    index = forCommand.validateEndFor(index);

                    varCommand.replaceVariablesWithValues(index);
                    varCommand.storeVariableInList(index);

                    ifCommand.validateIfStatementOneLine(index);
                    index = ifCommand.validateIfStatement(index);
                    index = ifCommand.validateEndIf(index);

                    comandsChecker.validateCommandsOfLine(index);
                }
                catch (Exception e)
                {
                    if (!(e.Message.Equals("Index was outside the bounds of the array.")))
                    {
                        errors = errors + "" +
                            "Line " + index + ": " + e.Message;

                    }
                }
            }
        }


        /// <summary>
        /// sets the string to be analised
        /// </summary>
        /// <param name="inputString"></param> takes a string of lines
        public void setLines(string inputString)
        {
            string[] separators = new string[] { "\r\n" }; //separates the lines by new line and returns chars
            lines = inputString.Split(separators, StringSplitOptions.None); //splits string into lines and store them in array
        }

        /// <summary>
        /// returns the errors of the string
        /// </summary>
        /// <returns></returns>
        public string getErrors()
        {
            return errors;
        }

        /// <summary>
        /// reset error variable and sets it to an empty string
        /// </summary>
        public void resetErrors()
        {
            errors = "";
        }
    }
}