﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{


    class CommandNotRecognised : Exception
    {
        string commmand;
        public CommandNotRecognised(string commmand)
        {
            this.commmand = commmand;

        }
        public override string Message
        {
            get
            {
                return commmand + " COMMAND NOT RECOGNISED";
            }
        }
    }
}
