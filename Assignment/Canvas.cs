﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Assignment
{
    /// <summary>
    /// class that has the information of the shapes drawn
    /// by the commands the user entered
    /// </summary>
    class Canvas
    {
        private ShapeFactory shapes = new ShapeFactory(); //creates an instance of ShapeFactory Class
        private Graphics g; //graphics to display the shapes
        private Pen pen; //the pen object for the line or outline of shape
        private Color color; //color of the shapes and lines

        private int positionX; //pen starting point position when drawing
        private int positionY; //pen starting point position when drawing
        private Shape shape; //s
        bool fillOnOff = false;//dictates where a fill should be activated for the next shape


        /// <summary>
        /// Constructor that takes graphics and set the pen positions
        /// </summary>
        /// <param name="g"> graphics object </param>
        public Canvas(Graphics g)
        {
            this.g = g;
            positionX = 0;
            positionY = 0; 
            this.color = Color.Black;
            pen = new Pen(color, 1); //creates a pen object Param(color, whickness)
        }

         /// <summary>
         /// draws a line
         /// </summary>
         /// <param name="toX"></param> position x  coordinates
         /// <param name="toY"></param> position y coordinates
        public void drawLine(int toX, int toY)
        {


            int[] parameters = { positionX, positionY, toX, toY }; //array that stores the positions and size of the rectangle
            try
            {

                shape = shapes.getShape("DrawTo", color, parameters); //gets the specific shape from the factory to be drawn

            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e);
            }
            shape.draw(g);//draws the shape to the graphics

        }

        /// <summary>
        /// draws a rectangle in the screen
        /// </summary>
        /// <param name="width"></param>the width of the rectangle
        /// <param name="height"></param> height of the rectangle
        public void drawRectangle(int width, int height)
        {
            int[] parameters = { positionX, positionY, width, height}; //array that stores the positions and size of the rectangle
            try
            {
                shape = shapes.getShape("Rectangle", color, parameters); //gets the specific shape from the factory to be drawn

            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e);
            }
            shape.draw(g);//draws the shape to the graphics

            if (fillOnOff == true)//if true is active, it will fill the shape
            {
                shape.fill();
            }
        }

        /// <summary>
        /// draws a Circle in the screen
        /// </summary>
        /// <param name="radius"></param> radius of the circle
        public void drawCircle(int radius)
        {
            int[] parameters = { positionX, positionY, radius }; //array that stores the positions and size of the Circle

            try { 
                shape = shapes.getShape("Circle", color, parameters); //stores shape object
            }
            catch (ArgumentException e)
            {
                Console.WriteLine("Invalid shape: " + e);
            }
            shape.draw(g);//draws in the screen

            if (fillOnOff == true) //if true, fills the shape
            {
                shape.fill();
            }
        }

        /// <summary>
        /// draws a Triangle in the screen
        /// </summary>
        /// <param name="point2x"></param> second x point of the coordinates
        /// <param name="point2y"></param> second y point of the coordinates
        /// <param name="point3x"></param> third x point of the coordinates
        /// <param name="point3y"></param> third y point of the coordinates
        public void drawTriangle(int point2x, int point2y, int point3x, int point3y)
        {
            int[] parameters = { positionX, positionY, point2x, point2y, point3x, point3y }; //array that stores the positions and size of the Triangle
            try { 
                shape = shapes.getShape("Triangle", color, parameters); //stores shape object
            }
            catch (ArgumentException e)
            {
                Console.WriteLine("Invalid shape: " + e);
            }
             shape.draw(g); //draws in the screen

            if (fillOnOff == true)//if true, fills the shape
            {
                shape.fill();

            }
        }

        /// <summary>
        /// method that sets the fillOnOff
        /// </summary>
        /// <param name="fillOnOff"></param> on or off parameter
        public void fill(bool fillOnOff)
        {
            if (fillOnOff == true)
            {
                this.fillOnOff = true;
            }else
            {
                this.fillOnOff = false;
            }
        }


        /// <summary>
        /// move the pointer to a certain location
        /// </summary>
        /// <param name="x"></param> x position to be moved
        /// <param name="y"></param> y position to be moved
        public void moveTo(int x, int y)  
        {
            positionX = x;
            positionY = y;
        }

        /// <summary>
        /// clears the outputWindow
        /// </summary>
        public void clearOutputWindow()
        {
            g.Clear(Color.FromArgb(105, 105, 105)); //background color grey
            
        }

        /// <summary>
        /// sets the color of the shape to be drawn
        /// </summary>
        /// <param name="color"></param> takes a string to be checked
        /// <exception>throws ArgumentException "color doesnt exist"</exception>
        public void setPenColor(string color)
        {
            //if input equals black, red, yellow, gree, or blue, it sets the color, otherwise, throw exception
            if (color == "BLACK") 
            {
                this.color = Color.Black;

            }
            else if (color == "RED")
            {
                this.color = Color.Red;
            }
            else if (color == "YELLOW")
            {
                this.color = Color.Yellow;
            }
            else if (color == "GREEN")
            {
                this.color = Color.Green;
            }else if (color == "BLUE")
            {
                this.color = Color.Blue;
            }
            else
            {
                System.ArgumentException argEx = new System.ArgumentException("Factory error: " + color + " is not a Colour");
                throw argEx;
            }
        }


        /// <summary>
        /// resets the position of the pen
        /// </summary>
        public void reset()
        {
            positionX = 0;
            positionY = 0;
        }

    }
}
