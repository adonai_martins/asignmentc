﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{
    /// <summary>
    /// class that handles the files
    /// </summary>
    class FileHandler
    {

        //C:\Users\user\source\repos\Assignment\Assignment\Properties\progress.txt
        private static string path = Directory.GetParent(Environment.CurrentDirectory).Parent.FullName + @"\Properties\";
        

        /// <summary>
        /// method that saves the program
        /// </summary>
        /// <param name="inputString"></param>
        public static void save(string inputString)
        {
            if (File.Exists(path + "progress.txt"))
            {
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(path + "progress.txt"))
                {
                    sw.WriteLine(inputString.Trim());
                }
            }
        }

        /// <summary>
        /// method that loads the program
        /// </summary>
        /// <returns></returns> returns the string of the data in the file
        public static string load()
        {
            // Open the file to read from.
            using (StreamReader streaReader = File.OpenText(path + "progress.txt"))
            {
                string s;
                s = streaReader.ReadToEnd(); // reads the file and saves it to a string
                return s;
            }
        }

        private static string ToLiteral(string input)
        {
            using (var writer = new StringWriter())
            {
                using (var provider = System.CodeDom.Compiler.CodeDomProvider.CreateProvider("CSharp"))
                {
                    provider.GenerateCodeFromExpression(new System.CodeDom.CodePrimitiveExpression(input), writer, null);
                    return writer.ToString();
                }
            }
        }

    }
}
