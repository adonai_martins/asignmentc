﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{
    class CommandsCheck
    {

        Canvas canvas;
        string[] lines;//stores the array of lines
        bool syntax = false;

        /// <summary>
        /// constructor that stores all the lines
        /// </summary>
        /// <param name="lines"></param> it takes an array of lines as a param
        public CommandsCheck(string[] lines, Canvas canvas)
        {
            this.lines = lines;
            this.canvas = canvas;
        }



        public void validateCommandsOfLine(int lineOffset)
        {
            lines[lineOffset] = lines[lineOffset].Trim().ToUpper(); //splits lines into commands and parameters and puts them in "lines"
            string[] LineBits = lines[lineOffset].Split(' '); //stores bits in lines
            string command = LineBits[0].ToUpper().Trim(); //stores the commands

            string[] LineBit2 = lines[lineOffset].Split('='); //stores bits in lines

         //   Console.WriteLine(lines[lineOffset]);



            if (LineBit2.Length == 1)
            {

                if (LineBits.Length == 1)// if there is only 1 string in the line///////////////////
                {
                    //  Console.WriteLine(command);
                    switch (command)
                    {
                        case "RESET": //if reset, reset the position of the pointer
                            canvas.reset();//resets the canvas
                            break;

                        case "CLEAR"://if clear clear the screen
                            canvas.clearOutputWindow();//clears the outputwindows
                            break;

                        default:
                            if (!command.Equals(""))
                            {
                                throw new ArgumentNullException(command + " COMMAND NOT RECOGNISED");
                            }
                            break;
                    }
                }
                else if (LineBits.Length == 2)//if there are two strings in the line/////////////////////////////
                {
                    string parameters = LineBits[1].ToUpper().Trim(); //stores the parameters cleaned
                    string[] individualParameters = parameters.Split(','); // stores individual parameters into an array

                    switch (command)
                    {
                        case "MOVETO":
                            try
                            {   //convert parameters strings to integerss
                                int parameter1 = Int32.Parse(individualParameters[0]);//
                                int parameter2 = Int32.Parse(individualParameters[1]);//
                                canvas.moveTo(parameter1, parameter2); //move cursor
                            }
                            catch (Exception e)
                            {
                                throw new ArgumentNullException("parameter must be a number or wrong parameter");
                            }
                            break;

                        case "DRAWTO":
                            try
                            {   //convert parameters strings to integerss
                                int parameter1 = Int32.Parse(individualParameters[0]);//
                                int parameter2 = Int32.Parse(individualParameters[1]);//
                                canvas.drawLine(parameter1, parameter2);//draws a line in the output window
                            }
                            catch (Exception e)
                            {
                                throw new ArgumentNullException("parameter must be a number or wrong parameter");
                            }
                            break;

                        case "RECTANGLE":
                            try
                            {   //convert parameters strings to integerss
                                int parameter1 = Int32.Parse(individualParameters[0]);//
                                int parameter2 = Int32.Parse(individualParameters[1]);///
                                canvas.drawRectangle(parameter1, parameter2);//draws rectangle to output windows
                            }
                            catch (Exception e)
                            {
                                throw new ArgumentNullException("parameter must be a number or wrong parameter");
                            }
                            break;

                        case "CIRCLE":
                            try
                            {
                                int parameter1 = Int32.Parse(individualParameters[0]);////convert parameters strings to integer


                                canvas.drawCircle(parameter1);//draw circle to the output windows
                            }
                            catch (Exception e)
                            {
                                throw new ArgumentNullException("parameter must be a number or wrong parameter");
                            }
                            break;

                        case "TRIANGLE":
                            try
                            {
                                //convert parameters strings to integerss
                                int parameter1 = Int32.Parse(individualParameters[0]);//
                                int parameter2 = Int32.Parse(individualParameters[1]);//
                                int parameter3 = Int32.Parse(individualParameters[2]);//
                                int parameter4 = Int32.Parse(individualParameters[3]);//
                                canvas.drawTriangle(parameter1, parameter2, parameter3, parameter4); //draws a triangle to the output windows
                            }
                            catch (Exception e)
                            {
                                throw new ArgumentNullException("parameter must be a number or wrong parameter");
                            }
                            break;

                        case "PEN":
                            try
                            {
                                canvas.setPenColor(parameters);//changes color of the pen of the next shape
                            }
                            catch (Exception e)
                            {
                                throw new ArgumentNullException("Parameter" + parameters + "should be a color(Yellow, Blue, Red, Green, Black)");
                            }
                            break;

                        case "FILL":
                            if (parameters == "ON")
                            {
                                canvas.fill(true);//turns the fill on
                            }
                            else if (parameters == "OFF")
                            {
                                canvas.fill(false);//turns the fill off
                            }
                            else
                            {
                                throw new ArgumentNullException("parameter" + parameters + "must be on/off");
                            }
                            break;
                        default:
                            if (!command.Equals(""))
                            {
                                throw new ArgumentNullException(command + " COMMAND NOT RECOGNISED");
                            }
                            break;
                    }//end of switch case

                }
                else if (LineBits.Length == 0 || LineBits.Length == 3)
                {

                }
                else
                {
                    throw new ArgumentNullException("wrong command");
                }
            }
            else if (LineBit2.Length == 2 && !(lines[lineOffset].Contains("if")))
            {

            }
            else {
                throw new ArgumentNullException("wrong Paremeter");
            }

        }//end of method


        public void checkSyntax(int lineOffset)
        {
            lines[lineOffset] = lines[lineOffset].Trim().ToUpper(); //splits lines into commands and parameters and puts them in "lines"
            string[] LineBits = lines[lineOffset].Split(' '); //stores bits in lines
            string command = LineBits[0].ToUpper().Trim(); //stores the commands

            string[] LineBit2 = lines[lineOffset].Split('='); //stores bits in lines

            //   Console.WriteLine(lines[lineOffset]);



            if (LineBit2.Length == 1)
            {

                if (LineBits.Length == 1)// if there is only 1 string in the line///////////////////
                {
                    //  Console.WriteLine(command);
                    switch (command)
                    {
                        case "RESET": //if reset, reset the position of the pointer
                            break;
                        case "CLEAR"://if clear clear the screen
                            break;
                        default:
                            if (!command.Equals(""))
                            {
                                throw new ArgumentNullException(command + " COMMAND NOT RECOGNISED");
                            }
                            break;
                    }
                }
                else if (LineBits.Length == 2)//if there are two strings in the line/////////////////////////////
                {
                    string parameters = LineBits[1].ToUpper().Trim(); //stores the parameters cleaned
                    string[] individualParameters = parameters.Split(','); // stores individual parameters into an array

                    switch (command)
                    {
                        case "MOVETO":
                            try
                            {   //convert parameters strings to integerss
                                int parameter1 = Int32.Parse(individualParameters[0]);//
                                int parameter2 = Int32.Parse(individualParameters[1]);//
                            }
                            catch (Exception e)
                            {
                                throw new ArgumentNullException("parameter must be a number or wrong parameter");
                            }
                            break;

                        case "DRAWTO":
                            try
                            {   //convert parameters strings to integerss
                                int parameter1 = Int32.Parse(individualParameters[0]);//
                                int parameter2 = Int32.Parse(individualParameters[1]);//
                            }
                            catch (Exception e)
                            {
                                throw new ArgumentNullException("parameter must be a number or wrong parameter");
                            }
                            break;

                        case "RECTANGLE":
                            try
                            {   //convert parameters strings to integerss
                                int parameter1 = Int32.Parse(individualParameters[0]);//
                                int parameter2 = Int32.Parse(individualParameters[1]);///
                            }
                            catch (Exception e)
                            {
                                throw new ArgumentNullException("parameter must be a number or wrong parameter");
                            }
                            break;

                        case "CIRCLE":
                            try
                            {
                                int parameter1 = Int32.Parse(individualParameters[0]);////convert parameters strings to integer
                            }
                            catch (Exception e)
                            {
                                throw new ArgumentNullException("parameter must be a number or wrong parameter");
                            }
                            break;

                        case "TRIANGLE":
                            try
                            {
                                //convert parameters strings to integerss
                                int parameter1 = Int32.Parse(individualParameters[0]);//
                                int parameter2 = Int32.Parse(individualParameters[1]);//
                                int parameter3 = Int32.Parse(individualParameters[2]);//
                                int parameter4 = Int32.Parse(individualParameters[3]);//
                            }
                            catch (Exception e)
                            {
                                throw new ArgumentNullException("parameter must be a number or wrong parameter");
                            }
                            break;

                        case "PEN":
                            try
                            {
                            }
                            catch (Exception e)
                            {
                                throw new ArgumentNullException("Parameter" + parameters + "should be a color(Yellow, Blue, Red, Green, Black)");
                            }
                            break;

                        case "FILL":
                            if (parameters == "ON")
                            {
                            }
                            else if (parameters == "OFF")
                            {
                            }
                            else
                            {
                                throw new ArgumentNullException("parameter" + parameters + "must be on/off");
                            }
                            break;
                        default:
                            if (!command.Equals(""))
                            {
                                throw new ArgumentNullException(command + " COMMAND NOT RECOGNISED");
                            }
                            break;
                    }//end of switch case
                }
                else if (LineBits.Length == 0 || LineBits.Length == 3)
                {
                }
                else
                {
                    throw new ArgumentNullException("wrong command");
                }
            }
            else if (LineBit2.Length == 2 && !(lines[lineOffset].Contains("if")))
            {
            }
            else
            {
                throw new ArgumentNullException("wrong command");
            }
        }//end of method
    }
}
